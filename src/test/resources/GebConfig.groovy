import org.openqa.selenium.Dimension
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.ie.InternetExplorerOptions
import org.openqa.selenium.remote.DesiredCapabilities

waiting {
    timeout = 5 //default
    retryInterval = 0.5

    //"named" timeout configurations
    slow { timeout = 10 }
    slower { timeout = 20 }
}


baseNavigatorWaiting = true
atCheckWaiting = true
reportOnTestFailureOnly = false
reportsDir = "target/execution-reports"

driver = {
    ChromeOptions options = new ChromeOptions()
    options.addArguments("--headless",'--disable-gpu','window-size=1024,768','--no-sandbox')
    newDriver = new ChromeDriver(options)
    return newDriver
}

//environments {
//
//    ff {
//        driver = { new FirefoxDriver() }
//    }
//
//    chrome {
//        ChromeOptions options = new ChromeOptions()
//        options.addArguments("--headless",'--disable-gpu')
//
//        driver = {new ChromeDriver(options) }
//    }
//
//    ie {
//        driver = {
//
//            def options = new InternetExplorerOptions()
//
//            def ieDriver = new InternetExplorerDriver(options)
//
//            ieDriver
//        }
//    }
//}
