package pages

import geb.Page

class DetailPage extends Page{
    static url="https://angular-crud-test-ffa326fa.herokuapp.com/detail";
    static at={
        nameValue.isDisplayed()
        cpfValue.isDisplayed()
        idValue.isDisplayed()
        ageValue.isDisplayed()
        deleteButton.isDisplayed()
        editButton.isDisplayed()
    };
    static content = {
        nameValue {$( "#title-view" )}
        ageValue {$( "#age-value" )}
        cpfValue {$( "#cpf-value" )}
        idValue {$( "#id-value" )}
        deleteButton {$("#delete-button")}
        editButton {$("#edit-button")}
    }
}
