package pages

import geb.Page

class EditPage extends Page {
    static url="https://angular-crud-test-ffa326fa.herokuapp.com/edit";
    static at={
        titleAdd.isDisplayed()
        ageInput.isDisplayed()
        cpfInput.isDisplayed()
        nameInput.isDisplayed()
        saveButton.isDisplayed()
    };
    static content={
        titleAdd {$( "#add-person" )}
        ageInput {$( "#age" )}
        cpfInput {$( "#cpf" )}
        nameInput {$( "#name" )}
        saveButton {$( "#save" )}
    }
}